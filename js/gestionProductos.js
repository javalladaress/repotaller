(function(win, endpoint) {

  var fn = {};

  var createElement = function(element) {
    return win.document.createElement(element);
  };

  fn.appendCell = function(data) {
    var cell = createElement("td");
    cell.innerText = data;
    this.appendChild(cell);
  };

  fn.appendCells = function(row, data) {
    data.forEach($.proxy(fn.appendCell, row));
  };

  fn.appendRow = function(tBody, data) {
    var row = createElement("tr");
    fn.appendCells(row, [
      data.ProductName, data.UnitPrice, data.UnitsInStock
    ]);
    tBody.appendChild(row);
  };

  fn.procesarProductos = function(data) {
    var values = data.value;
    var divTabla = win.document.querySelector("#divTabla");

    var tabla = createElement("table");
    var tBody = createElement("tbody");

    tabla.classList.add("table");
    tabla.classList.add("table-striped");

    values.forEach($.proxy(fn.appendRow, tabla, tBody));

    tabla.appendChild(tBody);

    divTabla.removeChild(divTabla.firstChild);
    divTabla.appendChild(tabla);
  };

  win.getProductos = function() {
    win.getFromAPI(endpoint, fn.procesarProductos);
  };

})(window, "https://services.odata.org/V4/Northwind/Northwind.svc/Products");
