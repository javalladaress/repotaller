(function(win, endpoint) {

  var fn = {};

  var createElement = function(element) {
    return win.document.createElement(element);
  };

  fn.getCountryImg = function(country) {
    country = (country == "UK")? "United-Kingdom" : country;
    return `https://www.countries-ofthe-world.com/flags-normal/flag-of-${country}.png`;
  };

  fn.appendFlag = function(row, country) {
    var cell = createElement("td");
    var flag = createElement("img");

    flag.src = fn.getCountryImg(country);
    flag.classList.add('flag');

    cell.appendChild(flag);
    row.appendChild(cell);
  };

  fn.appendCell = function(data) {
    var cell = createElement("td");
    cell.innerText = data;
    this.appendChild(cell);
  };

  fn.appendCells = function(row, data) {
    data.forEach($.proxy(fn.appendCell, row));
  }

  fn.appendRow = function(tBody, data) {
    var row = createElement("tr");
    fn.appendCells(row, [
      data.ContactName, data.Country, data.City
    ]);
    fn.appendFlag(row, data.Country);
    tBody.appendChild(row);
  };

  fn.procesarClientes = function(data) {
    var values = data.value;
    var divTabla = win.document.querySelector("#divTabla");

    var tabla = createElement("table");
    var tBody = createElement("tbody");

    tabla.classList.add("table");
    tabla.classList.add("table-striped");

    values.forEach($.proxy(fn.appendRow, tabla, tBody));

    tabla.appendChild(tBody);

    divTabla.removeChild(divTabla.firstChild);
    divTabla.appendChild(tabla);
  };

  win.getClientes = function() {
    win.getFromAPI(endpoint, fn.procesarClientes);
  };

})(window, "https://services.odata.org/V4/Northwind/Northwind.svc/Customers");
