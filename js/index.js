(function(win) {

  var fn = {};

  fn.respuestaRecibida = function(callback, response) {
    if( response.status == 200 ) {
      response.json().then(callback);
    }
  };

  win.getFromAPI = function(endpoint, callback) {
    fetch(endpoint).then($.proxy(fn.respuestaRecibida, callback, callback)).catch(console.error);
  };

})(window);
